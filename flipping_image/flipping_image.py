import os
from PIL import Image


class ImageFlipper:

    def __init__(self, img_path):
        if not os.path.isfile(img_path):
            raise Exception('Image file foes not exist')
        self.image = Image.open(img_path)

    def flip_image(self):
        l = 0
        r = self.image.width
        while l < r:
            l_coord = (l, 0, l + 1, self.image.height)
            r_coord = (r - 1, 0, r, self.image.height)

            right_column = self.image.crop(r_coord)
            left_column = self.image.crop(l_coord)

            self.image.paste(left_column, r_coord)
            self.image.paste(right_column, l_coord)

            l += 1
            r -= 1

        return self
