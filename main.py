import os
from flipping_image.flipping_image import ImageFlipper

for filename in os.listdir('./img'):
    name, extension = filename.split('.')
    if extension == 'jpg':
        flipper = ImageFlipper(os.path.abspath('img/%s' % filename))
        flipper.flip_image()
        flipper.image.save(os.path.abspath('img/%s_flipped.jpg' % name))
