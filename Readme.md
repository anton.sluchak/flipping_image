# Flipping image

## Task defined:
Create a tool that would flip the image sideways.

Usage of a tool - flipping video frames.

## Implementation
### Initial version
Columns of pixels should switch their position from left to right.

Similar to classical **reversing a string problem** , but instead of letters in a list of characters we reverse list of columns of pixels (1 x IMAGE_HEIGHT) in an image.

This implementation has a linear complexity O(n)

### Improvement hypothesis

Video consists of next parameters:
* Resolution
* Frame rate
* Bitrate

The hypothesis was that on lower bitrates it would be possible to use columns wider than 1 px

After investigation it became clear that frames do not get static horisontal pixel merge on lower rates, therefore the **hypothesis failed**.

 ## Installation and usage
 Install requirements:
 ```shell script
pip install -r requirements.txt
```
 
 You can see examples of usage in main.py [img](/img) folder contains the results of it's run
 The source pictures are [key frames](https://en.wikipedia.org/wiki/Key_frame) of FreshBooks introduction video from YouTube.
 
 ## Tests
 You can run tests using next command
 
 ```python
python -m unittest -v tests.flipping_test
```