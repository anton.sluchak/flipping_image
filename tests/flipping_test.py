import os
import unittest
from PIL import ImageChops
from flipping_image.flipping_image import ImageFlipper


class FlippingTest(unittest.TestCase):

    images = [
        'tests/assets/img_1280.jpg',
        'tests/assets/img_1279.jpg',
    ]

    def test_flipping(self):
        for image in self.images:
            image_path = os.path.abspath(image)
            self._flipping_image_success(image_path)
            self._double_flip_returns_same_image(image_path)

    def _flipping_image_success(self, image):
        self.assertTrue(True)
        flipper = ImageFlipper(image)
        initial_image = flipper.image.copy()
        flipper.flip_image()
        self.assertTrue(self._is_image_flipped(initial_image, flipper.image))

    def _double_flip_returns_same_image(self, image):
        self.assertTrue(True)
        flipper = ImageFlipper(image)
        initial_image = flipper.image.copy()
        flipper.flip_image()
        flipper.flip_image()
        self.assertTrue(self._is_same_image(initial_image, flipper.image))
        self.assertFalse(self._is_image_flipped(initial_image, flipper.image))

    @staticmethod
    def _is_image_flipped(img1, img2):
        if img1.width != img2.width or img1.height != img2.height:
            return False
        for x in range(img1.width // 2):
            left_stripe = img1.crop((x, 0, x + 1, img1.height))
            right_stripe = img2.crop((img2.width - x - 1, 0, img2.width - x, img2.height))
            diff = ImageChops.difference(left_stripe, right_stripe)
            if diff.getbbox():
                return False

        return True

    @staticmethod
    def _is_same_image(img1, img2):
        diff = ImageChops.difference(img1, img2)
        return not bool(diff.getbbox())

    def test_wrong_extension(self):
        with self.assertRaisesRegexp(IOError, 'cannot identify image file'):
            ImageFlipper(os.path.abspath('tests/assets/test.txt'))

    def test_file_does_not_esist(self):
        with self.assertRaisesRegexp(Exception, 'Image file foes not exist'):
            ImageFlipper(os.path.abspath('tests/assets/noFile.like.this'))


if __name__ == '__main__':
    unittest.main()
